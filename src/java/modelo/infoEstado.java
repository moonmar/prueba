/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.faces.model.SelectItem;

/**
 *
 * @author Dianita
 */
public class infoEstado 
{
private String nombreEstado;
private SelectItem[] ciudades;

public infoEstado(String nombreEstado,SelectItem...ciudades)
{
    this.nombreEstado=nombreEstado;
    this.ciudades=ciudades;
}

    public String getNombreEstado() {
        return(nombreEstado);
    }

    public SelectItem[] getCiudades() {
        return ciudades;
    }
    
    private static infoEstado[] ciudadesEstados={
        new infoEstado("Estado de Mexico",
                        new SelectItem("<i>desconocido</i>",
                                        "-----Selecciona ciudad-----"),
                        new SelectItem("30,073","Ixtapa de la Sal"),
                        new SelectItem("1,535,707","Toluca"),
                        new SelectItem("683.808","Tlanepantla")),
                new infoEstado("Jalisco",
                new SelectItem("<i>desconocido</i>",
                               "-----Selecciona ciudad-----"),
                new SelectItem("4,434,878","Guadalajara"),
                new SelectItem("57,104","San Juan de los Lagos"))
    };
public static infoEstado[] getCiudadesEstados()
{
    return(ciudadesEstados);
}
}
