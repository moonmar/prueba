/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author Dianita
 */
@ManagedBean(name="localidad")
@SessionScoped
public class LocalidadBean implements Serializable {

    /**
     * Creates a new instance of LocalidadBean
     */
    private String estado, ciudad;
    private boolean ciudadListaDeshabilitada;
    
    public LocalidadBean() 
    {
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public boolean isCiudadListaDeshabilitada() {
        return ciudadListaDeshabilitada;
    }

    public void setCiudadListaDeshabilitada(boolean ciudadListaDeshabilitada) {
        this.ciudadListaDeshabilitada = ciudadListaDeshabilitada;
    }
    public List<SelectItem> getEstados()
    {
        List<SelectItem> estados=new ArrayList();
        estados.add(new SelectItem("---Selecciona un Estado----"));
        for (infoEstado datosEstado:infoEstado.getCiudadesEstados()) {
            estados.add(new SelectItem(datosEstado.getNombreEstado()));
        }
        return(estados);
    }
    
    public SelectItem[] getCiudades()
    {
        SelectItem[] ciudades=
        {
            new SelectItem("---Selecciona una Ciudad----")};
        if (!ciudadListaDeshabilitada && (estado!=null)) {
            for (infoEstado datosEstado: infoEstado.getCiudadesEstados()) {
                if (estado.equals(datosEstado.getNombreEstado())) {
                    ciudades = datosEstado.getCiudades();
                    break;                   
                }               
            }         
        }
        return(ciudades);
        }    
}
