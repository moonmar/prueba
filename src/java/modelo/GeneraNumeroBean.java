/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Dianita
 */
@ManagedBean(name="genera")
@RequestScoped
public class GeneraNumeroBean {
private double numero=Math.random();
private double rango=1.0;

    public GeneraNumeroBean() {
    }
    public double getNumero() {
        return (numero);
    }

    public double getRango() {
        return rango;
    }

    public void setRango(double rango) {
        this.rango = rango;
    }

    public String aleatorizar()
    {
        numero=rango * Math.random();
        return (null);
    }
}
